import 'package:flutter/material.dart';

class ProfileStream extends StatelessWidget {
  const ProfileStream({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      child: PageView.builder(
          controller: PageController(viewportFraction: 0.8),
          itemCount: 3,
          itemBuilder: (_, i){
        return Container(
          height: 180,
          margin: EdgeInsets.only(right: 15, top: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: const DecorationImage(
              image:AssetImage("img/Pmd.png"),
            )
          ),
        );
      }),
    );
  }


}