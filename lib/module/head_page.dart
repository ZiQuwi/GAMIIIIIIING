import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Page/sign_in_interface.dart';
import '../Page/sign_up_interface.dart';
import '../utils/StringCallback.dart';

enum SimpleItem {register, connection}

class HeadPage extends StatefulWidget {

  //callback method 2
  const HeadPage({super.key, required this.updateParent, required this.toggleDrawer});
  final Function updateParent;

  //callBack Drawer
  final Drawercallback toggleDrawer;

  //callback method 1
  /*
  final StringCallback callback;
  MyChildClass({this.callback});
  */

  @override
  State<StatefulWidget> createState() => _HeadPAgeState();

}
class _HeadPAgeState extends State<HeadPage> {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  SimpleItem? selectedMenu;
  bool isMenu = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Container(
            alignment: Alignment.centerLeft,
            child: IconButton(
                icon: const Icon(Icons.menu),
                onPressed: () {
                  //envoie callback du drawer
                  widget.toggleDrawer();

                  //envoie du String au parent via callback method 2
                  if (isMenu) {
                    widget.updateParent("");
                    isMenu = false;
                  } else {
                    widget.updateParent("1");
                    isMenu = true;
                  }

                  //envoie du String au parent via callback method 1
                  /*
                  if (isMenu) {
                    callback("");
                    isMenu = false;
                  } else {
                    callback("1");
                    isMenu = true;
                  }
                   */
                }
            ),
          ),
        ),
        const Center(
          child: Text(
            "Home",
            style: TextStyle(
              fontFamily: 'TiltNeon',
              fontSize: 14,
              color: Colors.black,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            alignment: Alignment.centerRight,
            child: PopupMenuButton<SimpleItem>(
              initialValue: selectedMenu,
              onSelected: (SimpleItem item) => {
                setState(() {
                  if (item == SimpleItem.register) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const My_sign_up_interface()));
                  } else if (item == SimpleItem.connection) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const My_sign_in_interface()));
                  }
                })
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<SimpleItem>>[
                const PopupMenuItem<SimpleItem>(
                  value: SimpleItem.connection,
                  child : (Text(
                    "Connection"
                  )),
                ),
                const PopupMenuItem<SimpleItem>(
                  value: SimpleItem.register,
                  child : (Text(
                      "register"
                  )),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
  }
