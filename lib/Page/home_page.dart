import 'package:flutter/material.dart';
import 'package:sign_in_screen/module/Ads_homePage.dart';
import 'package:sign_in_screen/module/FIlter_homePage.dart';
import 'package:sign_in_screen/module/HomeDrawer.dart';
import 'package:sign_in_screen/module/head_page.dart';
import 'package:sign_in_screen/utils/StringCallback.dart';

import '../module/Profil_stream.dart';

class HomePage extends StatefulWidget{
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() => _HomeView();
}

class _HomeView extends State<HomePage> {

  List<String> adsStream = ["rrrrrrrrr", "hghghgh", "hhhhhhhh"];

  bool isOptionsAccount = false;
  
  //Return string from chidren method 2
  String valeurChildReturn = "";
  void updateValeurChildReturned(String value) {
    setState(() {
      valeurChildReturn = value;
      print(valeurChildReturn);
    });
  }

  //Method open Drawer
  final scaffoldKey = GlobalKey<ScaffoldState>();
  void _toggleDrawer() {
    setState(() {
      scaffoldKey.currentState!.openDrawer();
    });
  }

  @override
  void initState() {
    super.initState();
    initializeSelection();
  }

  //Initialise la liste au début de la construction de la page
  void initializeSelection() {
    adsStream = ["aaaaaaa", "eeeeeee", "gggggg", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ", "ZZ"];
  }

  //Clear The actual Stream
  @override
  void dispose() {
    adsStream.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(246, 241, 241, 1),
      child: SafeArea(
        child: Scaffold(
          key: scaffoldKey,

          //AppBar test pour ouvrir un drawer mais invocation drawer depuis enfant
          
          /*appBar: AppBar(
            centerTitle: true,
            title: const Text(
              'Home',
              style: TextStyle(
                fontFamily: 'TiltNeon',
                fontSize: 14,
                color: Colors.black
              ),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: const Icon(Icons.menu_rounded,
              color: Colors.black
              ),
              onPressed: (){
                print(scaffoldKey.currentState!.isDrawerOpen);
                if(scaffoldKey.currentState!.isDrawerOpen){
                  scaffoldKey.currentState!.closeDrawer();
                  //close drawer, if drawer is open
                }else{
                  scaffoldKey.currentState!.openDrawer();
                  //open drawer, if drawer is closed
                }
              },
            ),
          ),*/
          
          body: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  HeadPage(
                    //callBack method 2
                    updateParent: updateValeurChildReturned,
                    toggleDrawer: _toggleDrawer

                    //callback method 1
                    /*callback: (val) => setState(() => {
                      if (val == "1"){
                        scaffoldKey.currentState!.openDrawer()
                      } else {
                        scaffoldKey.currentState!.closeDrawer()
                      },
                      print(val),
                      print(scaffoldKey.currentState!.isDrawerOpen)
                    })*/
                  ),
                  const ProfileStream(),
                  FilterHomePage(),
                  Expanded(child: AdsHomePage(FilterList: adsStream))
                ]
            ),
        ),
          drawer: HomeDrawer(),
      )
     )
    );
  }

}