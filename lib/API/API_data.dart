import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'package:sign_in_screen/Classes/String_API.dart';

import '../Classes/User.dart';

class DataReturn{

  late String currentData;
  // Le ? permet de dire que la variable peut être null, cela permet d'instancier réellement la variable plus tard
  // ici elle est instanciée mais vaut null => correction error Field ... has not been initialized
  User? currentUser;

  Future<void> fetchData() async {
    var url = Uri.https('uppa.api.boavizta.org' , '/v1/server/all_default_models');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      List<dynamic> jsonResponse = jsonDecode(response.body) as List<dynamic>;
      List<String> tradList = List<String>.from(jsonResponse);
      List<StringAPI> allData = <StringAPI>[];
      for (var item in tradList) {
        allData.add(StringAPI.fromJson(item));
      }
      print("data retrieve from API : ${response.body}");
      _concatData(allData);
    } else {
      print("failed from retrieve data from API :  + ${response.statusCode}");
      String str = response.statusCode.toString();
      print("Data not found : $str");
    }
  }


  void _concatData(List<StringAPI> allData) {
    String str = "";
    allData.forEach((item) => {
      str += "/ ${item.testString} "
    });
    currentData = str;
  }

  String getStringFetchData() {
    return currentData;
  }

  Future<void> fetchDataODEPconnection(String username, String mdp) async {
    try {
      var url = Uri.http('90.84.244.69:4000' , '/oauth/api/v1.0.0/auth/sign_in');
      final headers = {'Content-Type': 'application/json', 'accept': 'application/json'};
      final body = json.encode({'userName': '$username', 'password': '$mdp'});
      print(body);
      var response = await http.post(url,headers: headers, body: body);
      print(response.body);
      if (response.statusCode == 200) {
        print(User.fromJson(jsonDecode(response.body)));
        final User user = User.fromJson(jsonDecode(response.body));
        print("---------------------------------------------------");
        print(user.Username);
        print(user.Email);
        currentUser = user;
        print("---------------------------------------------------");
      } else {
        throw Exception("failed to connect the user to database");
      }
    } catch (e) {
      print(e);
    }
  }

  //Future<void> fetchData
}
