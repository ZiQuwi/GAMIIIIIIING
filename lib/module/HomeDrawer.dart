import 'package:flutter/material.dart';
import 'package:sign_in_screen/Page/ODEP_test.dart';
import 'package:sign_in_screen/Page/Profile_page.dart';

class HomeDrawer extends StatelessWidget {
  const HomeDrawer({super.key});

  @override
  Widget build(BuildContext context) {

    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.white60,
            ),
            child: Text('Menu'),
          ),
          ListTile(
            title: const Text('Vente'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Messagerie'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('test ODEP'),
            onTap: () {
              // Then close the drawer
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) => ODEP_test()));
            },
          ),
          ListTile(
            title: const Text("Profile Page"),
            onTap: () {
              // Then close the drawer
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
            },
          )
        ],
      ),
    );
  }

}