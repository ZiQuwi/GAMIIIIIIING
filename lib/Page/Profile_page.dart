import 'dart:async';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_screen/Page/home_page.dart';
import 'package:sign_in_screen/module/LangagePref.dart';
import 'package:sign_in_screen/module/Profile_Confirm_Change.dart';

import 'dart:convert' as convert;
import '../Classes/User.dart';

class ProfilePage extends StatefulWidget {

  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final Future<SharedPreferences> _UserData = SharedPreferences.getInstance();
  bool _isModified = false;
  late String _Username = "";
  late String _FirstName = "";
  late String _LastName = "";
  late String _CreatedAt = "";
  late String _Email = "";

  /*
  TextEditingController? textEditingControllerUsername;
  TextEditingController? textEditingControllerFirstname;
  TextEditingController? textEditingControllerLastname;
  TextEditingController? textEditingControllerCreatedAt;
  TextEditingController? textEditingControllerEmail;
  */

  User? actualUser;

  /*
  @override
  void dispose() {
    textEditingControllerUsername?.dispose();
    textEditingControllerFirstname?.dispose();
    textEditingControllerLastname?.dispose();
    textEditingControllerCreatedAt?.dispose();
    textEditingControllerEmail?.dispose();
    super.dispose();
  }
  */

  Future<void> setactualUser() async {
    final prefs = await _UserData;
    final json = prefs.getString('LocaleUser');
    if (json != null) {
      print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      print("json of getUserUsername in Profile_page");
      print(json);
      final User userTamp = User.fromJson(convert.jsonDecode(json));
      print(userTamp.Email);
      actualUser = userTamp;
      print(actualUser!.Username);
      print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
  }

  /*Future<void> updateUser() async {
      throw Exception("eee");
  }*/

  Future<void> deleteactualUser() async {
    final prefs = await _UserData;
    prefs.remove('LocaleUser');
  }

  void Update_isModified(bool isModified) {
    setState(() {
      _isModified = isModified;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: SafeArea(
        child: Scaffold(
            body: Column(
              children: [
                 FutureBuilder(
                   future: setactualUser(),
                   builder: (builder,snapshot) {
                     if (snapshot.connectionState == ConnectionState
                         .waiting) { //While waiting for response return this
                       return const Center(
                           child: CircularProgressIndicator()
                       );
                     }
                     return Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           const SizedBox(height: 10),
                           const Center(
                             child: Text(
                               "Profil",
                               style: TextStyle(
                                   fontSize: 24,
                                   fontFamily: 'TiltNeon'
                               ),
                             ),
                           ),
                           const SizedBox(height: 20),
                           Container(
                               margin: const EdgeInsets.only(left: 10),
                               child :  Row(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     const Text("Username :     "),
                                     Expanded(
                                       child: TextField(
                                           controller: TextEditingController(text: actualUser!.Username),
                                           enabled: _isModified,
                                           onChanged: (text) {
                                             _Username = text;
                                           }
                                       ),
                                     )
                                   ]
                               )
                           ),
                           const SizedBox(height: 20),
                           Container(
                               margin: const EdgeInsets.only(left: 10),
                               child :  Row(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     const Text("First name :     "),
                                     Expanded(
                                       child: TextField(
                                           controller: TextEditingController(text: actualUser!.FirstName),
                                           enabled: _isModified,
                                           onChanged: (text) {
                                             _FirstName = text;
                                           }
                                       ),
                                     )
                                   ]
                               )
                           ),
                           const SizedBox(height: 20),
                           Container(
                               margin: const EdgeInsets.only(left: 10),
                               child :  Row(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     const Text("Last name :     "),
                                     Expanded(
                                       child: TextField(
                                           controller: TextEditingController(text: actualUser!.LastName),
                                           enabled: _isModified,
                                           onChanged: (text) {
                                             _LastName = text;
                                           }
                                       ),
                                     )
                                   ]
                               )
                           ),
                           const SizedBox(height: 20),
                           Container(
                               margin: const EdgeInsets.only(left: 10),
                               child :  Row(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     const Text("Email :     "),
                                     Expanded(
                                       child: TextField(
                                           controller: TextEditingController(text: actualUser!.Email),
                                           enabled: _isModified,
                                           onChanged: (text) {
                                             _Email = text;
                                           }
                                       ),
                                     )
                                   ]
                               )
                           ),
                           const SizedBox(height: 20),
                           Container(
                               margin: const EdgeInsets.only(left: 10),
                               child :  Row(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     const Text("Created on :     "),
                                     Expanded(
                                       child: TextField(
                                           controller: TextEditingController(text: actualUser!.CreatedAt),
                                           enabled: _isModified,
                                           onChanged: (text) {
                                             _CreatedAt = text;
                                           }
                                       ),
                                     )
                                   ]
                               )
                           ),
                           const SizedBox(height: 20),
                         ]
                     );
                   }
                 ),
                ProfileConfirmChange(Modified: Update_isModified, isModified: _isModified),
                  SizedBox(height: 40),
                  Container(
                      margin: const EdgeInsets.only(right: 15),
                      child: ElevatedButton(
                        onPressed: () {
                          // Action de déconnexion
                          Navigator.pop(context, MaterialPageRoute(
                          builder: (context) =>
                            HomePage()));
                          deleteactualUser();
                        },
                        child: Text('Logout'),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.grey, // Couleur d'arrière-plan grise
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20), // Bords arrondis
                          ),
                        ),
                      )
                  )
                ],
            ),
        )
      )
    );
  }


}