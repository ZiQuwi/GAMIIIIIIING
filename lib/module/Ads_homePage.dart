import 'package:flutter/material.dart';

class AdsHomePage extends StatefulWidget {
  AdsHomePage({super.key, required this.FilterList});
  List<String> FilterList;

  @override
  State<StatefulWidget> createState() => AdsHomePageState();

}

class AdsHomePageState extends State<AdsHomePage> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.FilterList.length,
      itemBuilder: (_, int index) {
        return ListTile(
          title: Text(widget.FilterList[index]),
        );
      },
    );
  }

}