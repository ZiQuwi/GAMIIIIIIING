import 'dart:ui';

import 'package:flutter/material.dart';

import '../main.dart';

class ButtonSign extends StatelessWidget {
  final String foText;
  const ButtonSign({Key? key, required this.foText}) : super (key:key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: [
            Colors.lightBlue,
            Colors.amber,
          ],
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: ElevatedButton(
          onPressed: () =>
          {
            // Change the text to "Sign" in the different langages
            if (MyApp.of(context)?.getLocale().toString() == 'en') {
              MyApp.of(context)?.setLocale(Locale('fr'))
            } else
              {
                MyApp.of(context)?.setLocale(Locale('en'))
              },
          },
          style: ElevatedButton.styleFrom(
            fixedSize: const Size(300,50),
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
          ),
          child: Text(
              foText,
              style: const TextStyle(
                  fontFamily: 'TiltNeon',
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
              ),
          ),
      ),
    );
  }
}