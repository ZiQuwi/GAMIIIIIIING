import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileConfirmChange extends StatefulWidget {
  ProfileConfirmChange({super.key, required this.Modified, required this.isModified});

  Function Modified;
  bool isModified;

  @override
  State<StatefulWidget> createState() => ProfileConfirmChangeState();

}

class ProfileConfirmChangeState extends State<ProfileConfirmChange> {


  @override
  Widget build(BuildContext context) {
    return widget.isModified == false ?
    Container(
      margin: const EdgeInsets.only(left:25),
      child: TextButton(
          onPressed: () => {
            widget.Modified(true)
          },
          child: const Text("Modifier",
              style: TextStyle(
                  color: Colors.deepOrange, fontFamily: 'TiltNeon'
              ))
      ),
    )
        : Container(
        margin: const EdgeInsets.only(left:25),
        child : Row (
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextButton(
                onPressed: () => {
                  widget.Modified(false)
                },
                child: const Text("Cancel",
                    style: TextStyle(
                        color: Colors.deepOrange, fontFamily: 'TiltNeon'
                    ))
            ),
            SizedBox(width:10),
            TextButton(
                onPressed: () => {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Confirmation'),
                        content: Text('Do you confirm your changements ?'),
                        actions: <Widget>[
                          TextButton(
                            child: Text('CANCEL'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15)
                            ),
                            child: ElevatedButton(
                              child: Text('CONFIRM'),
                              onPressed: () {
                                // Action de déconnexion
                                Navigator.of(context).pop();
                                widget.Modified(false);
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text('Les modifications ont été enregistrées.'),
                                    behavior: SnackBarBehavior.floating, // Rend la SnackBar flottante
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20), // Bords arrondis
                                    ),
                                    action: SnackBarAction(
                                      label: 'OK',
                                      onPressed: () {
                                        // Action à effectuer lorsque l'utilisateur appuie sur le bouton OK
                                      },
                                    ),
                                    backgroundColor: Colors.grey[800], // Couleur d'arrière-plan personnalisée
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20), // Bords arrondis
                        ),
                      );
                    },
                  )
                },
                child: const Text("Accepter la modification",
                    style: TextStyle(
                        color: Colors.deepOrange, fontFamily: 'TiltNeon'
                    ))
            ),
          ],
        )
    );
  }

}