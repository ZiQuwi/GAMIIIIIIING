import 'package:flutter/material.dart';

class FilterHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FilterHomePageState();

}

class FilterHomePageState extends State<FilterHomePage> {
  final myTextField1 = TextEditingController();
  final myTextField2 = TextEditingController();
  final myTextField3 = TextEditingController();
  bool _visibility = false;

  void setVisibility () {
    setState(() {
      _visibility= !_visibility;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          Container(
              height: 100,
              child: PageView.builder(
                  controller: PageController(viewportFraction: 0.5),
                  itemCount: 3,
                  itemBuilder: (_, i){
                    return Container(
                        height: 100,
                        width: 50,
                        margin: const EdgeInsets.only(right: 5, top: 20),
                        child: TextButton(
                          onPressed: () {},
                          child: const Text(
                              "Uzz"
                          ),
                        )
                    );
                  })
          ),
          TextButton(
            onPressed: () {
              setVisibility();
            },
            child: const Text(
                "Advanced options"
            ),
          ),
          Visibility(
            visible: _visibility,
            child: Container(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                height: 200,
                width: double.infinity,
                child: Column(
                  children: [
                    Row(
                        children: [
                          Expanded(
                            child: Row(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(right: 10),
                                    child: Text(
                                        "min price :"
                                    ),
                                  ),
                                  Expanded(
                                    child: TextField(
                                      controller: myTextField1,
                                      decoration: InputDecoration(
                                        hintText: "min price",
                                      ),
                                    ),
                                  )
                                ]
                            ),
                          ),
                          const SizedBox(width: 30),
                          Expanded(
                              child: Row(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(right: 10),
                                      child: Text(
                                          "max price :"
                                      ),
                                    ),
                                    Expanded(
                                      child: TextField(
                                        controller: myTextField2,
                                        decoration: InputDecoration(
                                          hintText: "max price",
                                        ),
                                      ),
                                    )
                                  ]
                              ))
                        ]
                    ),
                    Row(
                        children: [
                          Text(
                              "Filter"
                          ),
                          Expanded(
                            child: TextField(
                              controller: myTextField3,
                              decoration: InputDecoration(
                                hintText: "Filter",
                              ),
                            ),
                          )
                        ]
                    )
                  ],
                )
            ),
          ),
        ]
    );
  }
}
