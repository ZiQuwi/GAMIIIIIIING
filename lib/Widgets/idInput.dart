import 'package:flutter/material.dart';
import 'package:sign_in_screen/utils/StringCallback.dart';

class IDInput extends StatelessWidget {
  final String input;
  final StringCallback callback;

  const IDInput({
    required this.input,
    required this.callback,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(input),
        Container(
            margin: EdgeInsets.only(top: 15, right: 25, left: 25),
            child: TextField(
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Colors.black12,
                        width: 2
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Colors.grey,
                        width: 2
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  hintText: input
              ),
              onChanged: callback,
            )
        ),
      ],
    );
  }

}