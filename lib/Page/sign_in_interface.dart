import 'package:flutter/material.dart';
import 'package:sign_in_screen/Widgets/button_sign.dart';
import 'package:sign_in_screen/Widgets/idInput.dart';
import 'package:sign_in_screen/Page/sign_up_interface.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class My_sign_in_interface extends StatefulWidget {
  const My_sign_in_interface({super.key});

  @override
  State<StatefulWidget> createState() => _My_sign_in_interfaceState();
}

class _My_sign_in_interfaceState extends State<My_sign_in_interface> {

  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.height);
    return Container(
      color: const Color.fromRGBO(246, 241, 241, 1),
      width: double.infinity,
      height: MediaQuery.of(context).size.height,
        child: ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height,
            maxWidth: double.infinity
          ),
          child: SafeArea(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              body: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: const Text(
                          "Welcome",
                          style: TextStyle(fontSize: 50, fontFamily: 'TiltNeon')),
                    ),
                    const SizedBox(height: 30),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: IDInput(input: "Email", callback: (val) => {})
                      ),
                        Container(
                            child: IDInput(input: "Password", callback: (val) => {})
                        ),
                        const SizedBox(height: 30),
                        ButtonSign(foText: AppLocalizations.of(context).signUp),
                        GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => My_sign_up_interface()));
                            },
                            child: const Text(
                                "Sign up",
                                style: TextStyle(fontSize: 50, fontFamily: 'TiltNeon')
                            )
                        )],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
    );
  }
}