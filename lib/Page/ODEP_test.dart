import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;

import '../Classes/User.dart';
import '../Widgets/idInput.dart';
import '../API/API_data.dart';

class ODEP_test extends StatefulWidget {
  ODEP_test({super.key});

  @override
  State<StatefulWidget> createState() {
    return ODEP_testState();
  }
}

class ODEP_testState extends State<ODEP_test> {

  Future<SharedPreferences> _Userprefs = SharedPreferences.getInstance();
  DataReturn data = DataReturn();
  bool _LoginError = false;

  String? UsernameConnect;
  String mdpConnect = "";
  String UsernameInscription = "";
  String FirstnameInscription = "";
  String LastnameInscription = "";
  String RoleInscription = "";
  String EmailInscription = "";
  String mdpInscription = "";

  Future<void> saveUser(User user) async {
    print(user.FirstName + " " + user.LastName);
    final prefs = await _Userprefs;
    final json = convert.jsonEncode(user.ToJson());
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    print("json de SaveUser");
    print(json);
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    await prefs.setString('LocaleUser', json);
  }

  Future<void> getUserUsername() async {
    final prefs = await _Userprefs;
    final json = prefs.getString('LocaleUser');

    if (json != null) {
      print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      print("json de getUserUsername");
      print(json);
      print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      final map = convert.jsonDecode(json) as Map<String, dynamic>;
      UsernameConnect = User.fromJson(map).Username ?? "";
    }
  }

  void setLoginError(bool state) {
    setState(() {
      _LoginError = state;
    });
  }

  @override
  void initState() {
    super.initState();
    getUserUsername();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: SafeArea(
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                Column(
                  children: [
                    const Text("Text connexion compte ODEP"),
                    const SizedBox(
                      height: 10,
                    ),
                    IDInput(input: UsernameConnect != null ? UsernameConnect! : "username", callback: (val) => setState(() => {UsernameConnect = val})),
                    IDInput(input: "mdp", callback: (val) => setState(() => {mdpConnect = val})),
                    if (_LoginError)
                      const Text(
                        "Le nom d'utilisateur ou le mot de passe est incorrect",
                        style: TextStyle(color: Colors.red),
                      ),
                    TextButton(
                        onPressed: () async =>  {
                          await data.fetchDataODEPconnection(UsernameConnect!, mdpConnect),
                          if (data.currentUser != null) {
                            setLoginError(false),
                            saveUser(data.currentUser!),
                            getUserUsername()
                          } else {
                            setLoginError(true),
                          }
                        },
                        child: Text("Sign up to ODEP")
                    )
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Column(
                  children: [
                    const Text("Text inscription bd"),
                    const SizedBox(
                      height: 10,
                    ),
                    IDInput(input: "Username", callback: (val) => setState(() => {UsernameInscription = val, print(val)})),
                    IDInput(input: "FirstName", callback: (val) => setState(() => {FirstnameInscription = val, print(val)})),
                    IDInput(input: "LastName", callback: (val) => setState(() => {LastnameInscription = val, print(val)})),
                    IDInput(input: "Role", callback: (val) => setState(() => {RoleInscription = val, print(val)})),
                    IDInput(input: "Email", callback: (val) => setState(() => {EmailInscription = val, print(val)})),
                    IDInput(input: "mdp", callback: (val) => setState(() => {mdpInscription = val, print(val)})),
                    TextButton(
                        onPressed: () => {
                        },
                        child: Text("Sign in to ODEP")
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}