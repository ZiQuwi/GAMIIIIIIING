import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LangagePref extends StatefulWidget {
  const LangagePref({super.key});

  @override
  State<StatefulWidget> createState() {
    return LangagePrefState();
  }

}

class LangagePrefState extends State<LangagePref> {

  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  String dropdownValue = "English";

  Future<void> setLangage(String language) async {
    final langagePref = await prefs;
    await langagePref.setString('languageCode', language);
    print(langagePref.getString('languageCode'));
    langagePref.setString('countryCode', getLangagecode(language));
  }

  void changeLanguage(String language){
    setState(() {
      dropdownValue = language;
    });
  }

  Future<void> getLangage() async {
    final langagePref = await prefs;
    dropdownValue = (await langagePref.getString('Langage')) ?? "";
  }

  String getLangagecode(String Langagecode) {
    switch (Langagecode) {
      case 'en' :
        return "English";
      case 'fr' :
        return "Français";
      default :
        return "English";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  DropdownButton<String>(
        value: dropdownValue,
        onChanged: (String? newValue) {
            changeLanguage(newValue ?? "");
          print('Selected value: $dropdownValue');
        },
        items: <String>['English', 'Français']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }

}