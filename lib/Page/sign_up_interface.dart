import 'package:flutter/material.dart';
import 'package:sign_in_screen/API/API_data.dart';
import 'package:sign_in_screen/Page/sign_in_interface.dart';
import 'package:sign_in_screen/Widgets/button_sign.dart';
import 'package:sign_in_screen/Widgets/idInput.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class My_sign_up_interface extends StatelessWidget {
  const My_sign_up_interface({super.key});

  @override
  Widget build(BuildContext context) {
    DataReturn data = DataReturn();
    return Scaffold(
      body: FutureBuilder(
        future: data.fetchData(),
        builder: (builder,snapshot) {
          if (snapshot.connectionState ==
              ConnectionState.waiting){ //While waiting for response return this
            return const Center(
                child: CircularProgressIndicator()
            );
          }
          return Container(
              color: const Color.fromRGBO(246, 241, 241, 1),
              child: SafeArea(
                child: ConstrainedBox(
                    constraints: BoxConstraints(
                        maxWidth: MediaQuery
                            .of(context)
                            .size
                            .width,
                        maxHeight: MediaQuery.of(context).size.height
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Center(
                          child: Text(
                            "Register",
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Column(
                            children: [
                              IDInput(input: "your name", callback: (val) => {}),
                              SizedBox(height: 15),
                              IDInput(input: "your password", callback: (val) => {})
                            ]
                        ),
                        GestureDetector(
                            onTap: () {
                              Navigator.pop(context, MaterialPageRoute(
                                  builder: (context) =>
                                      My_sign_in_interface()));
                            },
                            child: const Text(
                                "Sign in",
                                style: TextStyle(
                                    fontSize: 12, fontFamily: 'TiltNeon')
                            )
                        ),
                        ButtonSign(foText: AppLocalizations.of(context).signUp),
                        Text(data.getStringFetchData())
                      ],
                    )
                ),
              )
          );
        }),
    );
  }
}